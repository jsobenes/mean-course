const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const postsRoutes = require('./routes/posts');

const app = express();

mongoose.connect('mongodb+srv://jsobenes:B9D0RvLIBEWAhaJ5@cluster0-trbga.gcp.mongodb.net/node-angular?retryWrites=true')
  .then(() => {
    console.log('Conectado a la BD');
  })
  .catch(() => {
    console.log('Falló la conexiona la BD')
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept',
  );
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PUT, PATCH, DELETE, OPTIONS'
  );
  next();
});

app.use('/api/posts', postsRoutes)

/* B9D0RvLIBEWAhaJ5
201.214.140.128 */

module.exports = app;
